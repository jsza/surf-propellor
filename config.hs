-- This is the main configuration file for Propellor, and is used to build
-- the propellor program.

import Propellor
import Propellor.Property.Scheduled
import qualified Propellor.Property.File as File
import qualified Propellor.Property.Apt as Apt
import qualified Propellor.Property.Ssh as Ssh
import qualified Propellor.Property.Sudo as Sudo
import qualified Propellor.Property.Systemd as Systemd
import qualified Propellor.Property.User as User
import qualified Propellor.Property.Hostname as Hostname
import qualified Propellor.Property.Docker as Docker

import qualified Propellor.Property.SiteSpecific.JayessSites as JayessSites
--import Data.List (unwords)

main :: IO ()
main = defaultMain hosts


data Region = USEast | USWest | SouthAmerica | Europe | Asia | Australia | MiddleEast | Africa | World
regionToString :: Region -> String
regionToString USEast = "0"
regionToString USWest = "1"
regionToString SouthAmerica = "2"
regionToString Europe = "3"
regionToString Asia = "4"
regionToString Australia = "5"
regionToString MiddleEast = "6"
regionToString Africa = "7"
regionToString World = "255"


-- The hosts propellor knows about.
-- Edit this to configure propellor!
hosts :: [Host]
hosts = [ aussurf
        , krsurf
        ]


aussurf :: Host
aussurf = host "aussurf.tempus.xyz" $ props
    & standardDockerSystem (Stable "buster") X86_64
    & ipv4 "45.32.242.254"
    & gameServer "AU66" "#AusSurf Skill [66Tick - Tricks - FastDL]" Australia "surf_syria" "16" "27015" "150"
    & gameServer "AU100" "#AusSurf Skill [100Tick - Tricks]" Australia "surf_smaragd" "16" "27020" "1000"


krsurf :: Host
krsurf = host "svc2.kvmh.net" $ props
    & standardDockerSystem (Stable "jessie") X86_64
    & ipv4 "61.84.12.67"
    & gameServer "KR" "#AusSurf Korea [66Tick - Tricks]" Asia "surf_syria" "32" "27015" "150"
    & ("/srv/css_KR/cstrike/addons/sourcemod/configs/admins_simple.ini")
        `File.hasContent`
            [ unwords ["STEAM_0:0:14265062", "99:z"]           -- jayess
            , unwords ["STEAM_0:0:37980208", "49:bcdefgjkntm"] -- zanz
            , unwords ["STEAM_0:0:4388313",  "49:bcdefgjktnm"] -- adzicents
            , unwords ["STEAM_0:1:53669339", "25:bcdefgjknm"]  -- undr
            , unwords ["STEAM_0:0:42607036", "g"]              -- zato
            , unwords ["STEAM_0:1:40072001", "50:z"]           -- Nyang
            , unwords ["STEAM_0:1:43684038", "50:bcdefgjkn"]   -- Shift
            , unwords ["STEAM_0:1:32004836", "50:z"]           -- FINGER
            ]
    & Ssh.noPasswords


secureDockerSystem :: DebianSuite -> Architecture -> Property (HasInfo + Debian)
secureDockerSystem suite arch = propertyList "secure docker system" $ props
    & standardDockerSystem suite arch
    & Ssh.randomHostKeys
    & Ssh.noPasswords


standardDockerSystem :: DebianSuite -> Architecture -> Property (HasInfo + Debian)
standardDockerSystem suite arch = propertyList "standard docker system" $ props
    & standardSystem suite arch
    & Docker.installed
    & Docker.garbageCollected `period` Daily
    & propertyList "admin docker access"
    (toProps (flip User.hasGroup (Group "docker") <$> admins))


standardSystem :: DebianSuite -> Architecture -> Property (HasInfo + Debian)
standardSystem suite arch = propertyList "standard system" $ props
    & osDebian suite arch
    & Hostname.sane
    & Hostname.searchDomain
    & Apt.stdSourcesList
    & Apt.installed [ "openssh-server"
                    , "openssh-client"
                    , "git"
                    , "ca-certificates"
                    , "htop"
                    , "less"
                    , "curl"
                    , "atool"
                    , "vim"
                    , "fish"
                    , "rsync"
                    , "conntrack"
                    ]
    & Apt.removed  ["needrestart"]
    & Apt.serviceInstalledRunning "ntp"
    & Ssh.permitRootLogin (Ssh.RootLogin True)
    & Apt.installed ["sudo"]
    & propertyList "admin accounts"
      (toProps $
        [ User.accountFor
        , User.lockedPassword
        , setupRevertableProperty . Sudo.enabledFor
        ] <*> admins)
    & adminKeys (User "root")
    & jayessKeys (User "jayess")
    & adzicentsKeys (User "adzicents")
    & User.shellSetTo (User "jayess") "/usr/bin/fish"
    & User.shellSetTo (User "adzicents") "/usr/bin/fish"
    & File.notPresent "/etc/cron.d/propellor"
    & File.notPresent "/etc/cron.d/tempus_map_updater"
    & mapUpdater "/srv/tempus_maps"


mapUpdater :: String -> Property Linux
mapUpdater mapsPath = propertyList "map updater" $ props
    & File.dirExists "/srv/tempus_maps"
    & JayessSites.ownerGroupRecursive "/srv/tempus_maps" (User "5000") (Group "5000")
    & ("/etc/systemd/system/" ++ serviceName ++ ".service") `File.hasContent`
        [ "[Unit]"
        , "Description=Tempus Map Updater"
        , "After=docker.service"
        , "Requires=docker.service"
        , ""
        , "[Service]"
        , "Restart=always"
        , ""
        , "ExecStartPre=-/usr/bin/docker kill " ++ serviceName
        , "ExecStartPre=-/usr/bin/docker rm " ++ serviceName
        , "ExecStartPre=/usr/bin/docker pull " ++ imageName
        , "ExecStartPre=/usr/bin/docker create --name " ++ serviceName ++ " "
          ++ "--interactive --tty "
          ++ "-v " ++ mapsPath ++ ":/srv/tempus_maps "
          ++ imageName ++ " "
          ++ "--notify-server 'wss://tempus.xyz:9000' "
          ++ "--notify-realm 'surf' "
          ++ "--maps-path '/srv/tempus_maps' "
          ++ "--delete "
          ++ "s3 "
          ++ "--fetch-url 'https://dalq9uywf1zlx.cloudfront.net/cstrike/maps/' "
          ++ "--list-url 'http://surf.jayess.za.net.s3.amazonaws.com/' "
          ++ "--key-prefix 'cstrike/maps/'"
        , "ExecStart=/usr/bin/docker start --attach " ++ serviceName
        , "ExecStopPost=-/usr/bin/docker kill " ++ serviceName
        , ""
        , "[Install]"
        , "WantedBy=multi-user.target"
        ]
        `onChange` Systemd.daemonReloaded
    & Systemd.enabled (serviceName)
    & Systemd.started (serviceName)
    where
        serviceName = "map_updater"
        imageName = "jayess/tempus-new-map-updater"


gameServer :: String -> String -> Region -> String -> String -> String -> String -> Property (HasInfo + DebianLike)
gameServer shortname title region defaultMap maxPlayers port airAccel = propertyList "game server" $ props
    & defaultServerConfig shortname title region airAccel
    & gameService shortname defaultMap maxPlayers port


gameService :: String -> String -> String -> String -> Property (HasInfo + DebianLike)
gameService shortname defaultMap maxPlayers port = propertyList "game service" $ props
    & JayessSites.accountForUid (User "steam-docker") "5000"
    & User.lockedPassword (User "steam-docker")
    & adminKeys (User "steam-docker")
    & File.dirExists serverPath
    & File.dirExists tempusCfgPath
    & (tempusCfgPath ++ "maptype") `File.hasContent` ["surf"]
    & File.hasPrivContent (tempusCfgPath ++ "db.yml") hostContext
    & File.hasPrivContent (tempusCfgPath ++ "s3.yml") hostContext
    & JayessSites.ownerGroupRecursive serverPath (User "5000") (Group "docker")
    & File.dirExists "/srv/tempus_maps"
    & JayessSites.ownerGroupRecursive "/srv/tempus_maps" (User "5000") (Group "5000")
    & JayessSites.symbolicLink ("/srv/tempus_maps") (serverPath ++ "/cstrike/custom/tempus/maps")

    & ("/etc/systemd/system/" ++ serviceName ++ ".service") `File.hasContent`
        [ "[Unit]"
        , "Description=Tempus TF2 Jump Server (" ++ shortname ++ ")"
        , "After=docker.service"
        , "Requires=docker.service"
        , ""
        , "[Service]"
        , "Restart=always"
        -- , "StandardInput=tty-force"
        , ""
        , "ExecStartPre=-/usr/bin/docker kill " ++ serviceName
        , "ExecStartPre=-/usr/bin/docker rm " ++ serviceName
        , "ExecStartPre=/usr/bin/docker pull jayess/docker-tempus-css"
        , "ExecStartPre=/usr/bin/docker create --name " ++ serviceName ++ " --interactive -p " ++ port ++ ":" ++ port ++ "/udp --tty -v " ++ serverPath ++ ":/srv/srcds -v /srv/tempus_maps:/srv/tempus_maps jayess/docker-tempus-css -port " ++ port ++ " +map " ++ defaultMap ++ " -maxplayers " ++ maxPlayers ++ " -norestart " ++ "-tickrate 100"
        , "ExecStart=/usr/bin/docker start --attach " ++ serviceName
        , "ExecStartPost=-/bin/sh -c 'sleep 5; /usr/bin/docker inspect -f \"{{ .NetworkSettings.IPAddress }}\" " ++ serviceName ++ "> /run/docker-ip-" ++ serviceName ++ "'"
        , "ExecStopPost=-/usr/bin/docker kill " ++ serviceName
        , "ExecStopPost=-/bin/sh -c 'conntrack -D -r $(cat /run/docker-ip-" ++ serviceName ++ ")'"
        , ""
        , "[Install]"
        , "WantedBy=multi-user.target"
        ]
        `onChange` Systemd.daemonReloaded

    & (serverPath ++ "/cstrike/addons/sourcemod/configs/admins_simple.ini")
        `File.hasContent`
            [ unwords ["STEAM_0:1:487211",   "51:z"]           -- Deceiver
            , unwords ["STEAM_0:0:16906962", "50:bcdefgjktnm"] -- illusive
            , unwords ["STEAM_0:1:53767778", "99:z"]           -- will
            , unwords ["STEAM_0:0:30739476", "49:bcdefgjktnm"] -- Mystery
            , unwords ["STEAM_0:0:14265062", "99:z"]           -- jayess
            , unwords ["STEAM_0:0:37980208", "49:bcdefgjkntm"] -- zanz
            , unwords ["STEAM_0:0:4388313",  "99:bcdefgjktnm"] -- adzicents
            , unwords ["STEAM_0:1:53669339", "25:bcdefgjknm"]  -- undr
            , unwords ["STEAM_0:0:42607036", "g"]              -- zato
            ]
    & Systemd.enabled (serviceName)
    & Systemd.started (serviceName)

    where
        serviceName = "css_" ++ shortname
        serverPath = "/srv/" ++ serviceName
        tempusCfgPath = serverPath ++ "/cstrike/addons/source-python/plugins/tempus_loader/local/cfg/"


defaultServerConfig :: String -> String -> Region -> String -> Property (HasInfo + UnixLike)
defaultServerConfig shortname title region airAccel = propertyList "srcds config files" $ props
    & File.dirExists (cfgPath)
    & File.hasPrivContent (cfgPath ++ "server_account.cfg") hostContext
    & (cfgPath ++ "autoexec.cfg") `File.hasContent`
        [ "exec server_account.cfg"
        , "sp plugin load tempus_loader"
        , "sv_maxvelocity 3500"
        , "tv_enable 1"
        ]
    & (cfgPath ++ "server.cfg") `File.hasContent`
        [ "exec tempus_defaults.cfg"
        , "exec tempus_server.cfg"
        , "exec tempus_override.cfg"
        ]
    & (cfgPath ++ "tempus_defaults.cfg") `File.hasContent`
        [ "sv_allowdownload 0"
        , "sv_alltalk 1"
        , "sv_cheats 0"
        , "sv_downloadurl \"http://dalq9uywf1zlx.cloudfront.net/cstrike/\""
        , "sv_enablebunnyhopping 1"
        , "sv_hudhint_sound 0"
        , "sv_maxvelocity 3500"
        , "mp_allowspectators 1"
        , "mp_autokick 1"
        , "mp_autoteambalance 0"
        , "mp_flashlight 1"
        , "mp_forcecamera 0"
        , "mp_freezetime 0"
        , "mp_friendlyfire 0"
        , "mp_ignore_round_win_conditions 1"
        , "mp_limitteams 0"
        , "mp_startmoney 16000"
        , "mp_timelimit 40"
        , "tv_enable 1"
        , "tv_allow_camera_man 0"
        , "tv_allow_static_shots 0"
        , "tv_autorecord 0"
        , "tv_chattimelimit 2"
        , "tv_delay 120"
        , "tv_name \"AusSurf SourceTV\""
        , "tv_transmitall 1"
        ]
    & (cfgPath ++ "tempus_server.cfg") `File.hasContent`
        [ "hostname \"" ++ title ++ "\""
        , "sv_region " ++ (regionToString region)
        , "sv_password \"\""
        , "sv_airaccelerate " ++ airAccel
        ]

    where
        cfgPath = "/srv/css_" ++ shortname ++ "/cstrike/custom/tempus/cfg/"


admins :: [User]
admins = map User ["jayess", "adzicents"]


jayessKeys :: User -> Property UnixLike
jayessKeys user = propertyList "keys for jayess"
                  . toProps
                  . map (setupRevertableProperty . Ssh.authorizedKey user) $
                  [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsdT1r84ZlfcHyibUUZqrT1SkJ5JNBVd/Uwym4T2L2okUqqet+a+oxzIyclN2/9ay8hdk+Fmh4uro97F0Mu7OxX/K8qsa/C6B1wLWdYB4BrbHxhPs8edklPRVB0QaepYNtUZWvvb+TOekCRaG9KTYatAcd/0hfu6qiAk2RvIOJk0g8o2WWhIKoUUYUrAUuoBQhAhOtfcZGjriIiMWjkX0+NpGx6twpgQvQNYy7w39NrgP0IhnVd1gJw6mSIwCQM0p3ztvxI2x0Iwi8oFSq6UwZnHVIs7b7Gms+rv+Q7yXvCYaYvFBnM3a8VuVc7WFJ7CwEGxqtb+PBneeDzd/z2OeF cardno:000603012235"
                  ]


adzicentsKeys :: User -> Property UnixLike
adzicentsKeys user = propertyList "keys for adzicents"
                     . toProps
                     . map (setupRevertableProperty . Ssh.authorizedKey user) $
                     [ "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAvK0UiPnVB+FP2/KpejQG3U2DFv3ZD3+dnFEqD6dKSwGlbAD357QEnDZpIWef8ieaR4y5WStX7qMmRm4ctEKDYdZFZyc3Urdy/hju83W4DQNACJfxLYR+3d4O/6ciUZs/LgSd/0JgnbJANdmDIWbOoHWGtXu9HEcTsVoBK5rM3tE= rsa-key-20160921"
                     ]


adminKeys :: User -> Property UnixLike
adminKeys user = propertyList "admin keys" . toProps . map ($ user) $
                 [ jayessKeys
                 , adzicentsKeys
                 ]
